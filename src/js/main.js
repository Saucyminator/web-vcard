---
---

'use strict';

$(function () {
  var output = [];
  var categories = $('#categories');
  var input = $('#input');
  var button = $('#generate');
  var generated = $('#data');
  var template = jQuery.validator.format("BEGIN:VCARD\nVERSION:3.0\nFN:{0} {1}\nN:{1};{0};;;\nTEL;TYPE=WORK:{2}\nCATEGORIES:{3}myContacts\nEND:VCARD")

  function generateList() {
    if (output.length > 0) {
      output = [];
    }

    var lines = input.val().split(/\n/);

    for (let index = 0; index < lines.length; index++) {
      output[index] = [];
      var splitLine = lines[index].split(",");

      if (/\S/.test(splitLine[index])) {
        output[index] = splitLine.map(el => el.trim());
      }
    }
  };

  button.on('click', function () {
    if (!$(input).val()) {
      return;
    }

    generateList();
    generated.empty();

    var splitCategories = categories.val().split(",");
    var formattedCategories = [];

    for (let index = 0; index < splitCategories.length; index++) {
      formattedCategories.push($.trim(splitCategories[index]));
    }

    $.each(output, function (index) {
      $(generated).append(template(output[index][0], output[index][1], output[index][2], formattedCategories) + '<br>');

      if (index === (output.length - 1)) {
        $(generated).append('<br>');
      }
    });
  });
});
